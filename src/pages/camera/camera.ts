import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the CameraPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-camera',
  templateUrl: 'camera.html',
})
export class CameraPage {

  color: string;
  
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    
    //initialize ezAR videoOverlay plugin and start back camera
    var win: any = window;
    if (win.ezar) {
      var ezar: any = win.ezar;
      ezar.initializeVideoOverlay(
        function () {
          ezar.getBackCamera().start();
        },
        function (err) {
          console.error(err);
        }, { fitWebViewToCameraView: true });
    } else {
      alert('Unable to detect the ezAR plugin');
    }

  }

  ionViewDidEnter(){
    this.color = "transparent";    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CameraPage');
  }

}
